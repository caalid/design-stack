# Sketch Plugins

### Anima Toolkit
I use this for the pinning feature, which lets you create stacks to emulate a fluid, responsive layout in Sketch. Somewhat complex, but very powerful.

### Runner
Runner lets you execute quick, type-based commmands in Sketch, similar to how Spotlight search works. You can run plugin commands, insert symbols, and much more.

### Notes
Notes is a tiny plugin that lets you drop in little, styled notes into your Sketch docs.

### Abstract
Abstract needs a plugin + its standalone Mac app to do version control.

### Artboard Framer
A somewhat-useful plugin to wrap artboards in device frames. Helpful when exporting screens for placement in a deck, for example.

### Artboard Tools
A plugin that has several utilities for arranging, rearranging and more with artboards.

### Automate Sketch
A Swiss army knife of Sketch goodness and shortcuts. Tons of stuff in here, great keyboard shortcuts, and way more.

### Butter
A **very** useful plugin that lets you space out multiple elements with typed pixel amounts.

### Character Count
A helpful little utility to count the characters in a text box.

### Craft
Craft syncs your artboards to InVision prototypes, and has a ton of helpful features like a duplicator, a way to insert stock photos from Unsplash, and a way to insert various pre-made text strings. Plus, you can insert your very own JSON file of key-value text data -- like product data with titles, prices, and other metadata.

### Framer Pasteboard
Framer Pasteboard lets you paste Sketch assets directly into Framer as live elements, not rastered images.

### Icons
A plugin that allows you to insert icon fonts. Especially useful because we use Fontawesome as an icon font on cuna.org

### Paddy
Paddy allows you to dictate padding for an element inside of a parent container. This is especially useful for buttons -- since you can keep the padding around the button text consistent as you change the text.

### Place Linked Bitmap
This plugin lets you place linked bitmap files, in the same way that Illustrator or InDesign works. When you update the file in Finder, you can run a command to automatically update all the linked bitmaps.

### Rename-It
A handy plugin to batch rename multiple layers, groups, or artboards. You can run sequences or insert different merge fields into the names.

### Sketch Guides
A **very** handy plugin to place guides based on the selected object.

### Sketch Measure
A plugin that can add redline specs to your Sketch file, and even export it's own InVision Inspect-like files for developer handoff.

### Sketch2AE
A plugin that lets you paste live elements, include live text, from Sketch to After Effects.

### Stark
A **very** handy plugin that can check the contrast of text against a background and let you know if it meets WCAG 2.0 AA or AAA rating for accessibility.

### Swatches
Generates swatches from colors and styles.

### Symbol Organizer
Organizes your symbols page automatically and can group symbols together based on different criteria.

### Text Box Fit Content
A handy little plugin that will resize a text box to fit the text that's actually inside of it.

### Upload to Wake
Used to upload artboards to Wake.

### User Flows
A plugin that generates visual user flows and can automatically create a document out of them in a new page in Sketch.

### Overflow
A plugin that lets you sync Sketch artboards to Overflow to create easily create interactive, shareable user flows.