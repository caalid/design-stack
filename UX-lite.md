# Overview

A canvas for approaching bite-sized, UI or UX issues, primarily on Ektron Microsites. This assumes that the turnaround will be somewhat quick, and the main plane of manipulation is with existing Ektron widgets.

## Remember:
- Design is an atitude more than a job. Anyone is a designer if they're rendering intent and changing the user's experience.
- Removing friction is the goal, which sometimes means taking something away rather than adding.
- Have a reason for why you make a design change.
- Lean principles are helpful. Make a change, measure, adapt, and repeat. Sometimes *not* changing the page, but rather just observing changes in beahvior, is the best path forward.
- Plan for multiple paths, not just conversions. What if the user just wants information? Not everything is a checkout, and customer journeys are not always linear.

# Phase One :: Discovery

**Goal:** Dive into what the problem is, who it affects, and what the goal is.

**Inputs for this Phase:**

- The stakeholder or stakeholders who will own this from Marketing, and any relevant stakeholders from the Business.
- An overview of some sorts from the stakeholders on the problem.

**Activities:**

- If possible, a short kick-off meeting. Even 30-minutes in a room with a whiteboard can help get everyone on the same page.
- IF the issue is looking like it's larger than can be solved with existing Ektron widgets, engage Kreshnik and/or Nicole.

**Key Questions:**

- Who is this for? What persona or audience do they fit into?
- What context are they in? Where are they? What device are they in?
- Where have they just come from? An email? Social media?
- What's the intended outcome for the user? Is it actionable, like a purchase? Or more informational, like finding out information about a hotel for an event? What might be an obstacle to that?
- Where does this specific project fit into the larger way people interact with this page or microsite now?
- Where are user pain points in the current way people accomplish the task?
- What assumptions exist? Challenge, or at least highlight what's an assumption, and think about how that assumption could be validated.
- What does success look like? What measures will we use to track it?
- Ask stakeholders to answer things like:
  - I believe my users have a need to ____
  - I believe these needs can be solved by ____
  - I believe that the #1 thing my user needs to get from this is ____
  - I believe the biggest risk is ____

**Outputs from this Phase:**

- A doc which clearly outlines the problem statement  and can function as a source of truth. It could also include things like:
  - A defined scope — what will and what won't be solved. These should be prioritized items that are clearly defined.
  - A defined stakeholder or stakeholders, and a list of others that should be informed.
  - A rough timetable for when stakeholders will see outputs of future phases, and things like wireframes, mockups, and a testable design solution on the dev or staging sites.
- Socialize this doc with the project stakeholders so that everyone is in agreement. I would recommend looping in Kreshnik for visibility but a full checkpoint might not be needed at this stage.

# Part Two :: Understand & Prototype

**Goal:** Understand the problem, and draft a way forward. We now have a picture of what issues the user is facing, and where the current experience is lacking. Great! Let's understand and explore that.

**Activities:**

- Engage Nick with an analytics request to help answer qualitative questions and measures for success.

**Key Questions:**

- Do we know enough to solve this? The output from this request might simply have to be either:
  - We wait and see by placing more comprehensive analytics on the page and measuring them again over the next weeks or months.
  - We experiment to answer the question "What's the easiest way to learn the next most important thing we need to know?"
- Reframe the problem. What if all of the assumptions are wrong?

**Outputs from this Phase:**

- A hypothesis that translates the issues into things to build:
  - *If we do X we'll see Y change in user behavior, and we'll know because Z*
- Extra credit: transform these into basic, ad-hoc User Stories:
  - *As a {persona} I want to {action}, so I can {goal}*
- An updated doc with your hypothesis and supporting data, sent to everyone on the project. Update the stakeholder on what's next and on any snags. Ask more questions, be vulnerable, and admit when you don't know what to do to solve the problem or if you don't have enough data to validate assumptions.

## **Checkpoint!**

At this point, check-in with Kreshnik, or even better, the whole Digital Team, and present your findings so far and get feedback.

# Part Three :: Design

**Goal:** Create an easy mockup or plan for what you'll build before you'll build it, so that stakeholders can understand what you're making, and how it solves the problem.

**Key Activities:**

- Use a tool like Balsamiq, or refine your sketches and use your iPhone to import them into your computer.
- Review your mockups with stakeholders, ideally in a short meeting (even 30-minutes is helpful!). You may want to do a check-in with Kreshnik or Nicole first. Make sure to note why you made your decisions, and emphasize how these design decisions will help solve the issues they've raised. After the meeting, give the stakeholders the deliverable (Balsamiq link, InVision link, or a PDF deck with annotations) that explains the designs so they can review them again, or share them with relevant stakeholders who were not at the meeting.
- You may have to ideate based on stakeholder feedback. This might loop several times, and that's okay!
- Form a plan to measure your design with Nick. This might require him to manipulate Google Tag Manager and/or create a report. He'll know what to do. The important thing is to communicate.
- Your design is accessible!

**Outputs from this Phase:**

- You've got alignment from everyone on what will be built and a clear path forward. They've already given you feedback so that you are limiting any re-work that might occur after you build the page, or pages, in Ektron.
- You've got alignment with Nick on how we'll be measuring for success, and the stakeholders understand.
- You might have additional user stories or issues that you've mocked up but will build later.

# Part Four :: Build

**Goal:** Take your design from wireframes to production by building it using Ektron widgets and/or existing styling.

**Key Activities:**

- Translate designs from wireframes into Ektron widgets.
- Give project stakeholders a preview in staging and incorporate any small final changes.
- Schedule a check-in in the future to review any analytics to make adjustments.
- Update any documentation as needed.

**Outputs from this Phase:**
- A completed page or multiple pages in Ektron staging.
- Feedback from your stakeholders that you've already looped through to iterate your design.
- Alignment with Nick on what measures will be tracked after you ship.
- A date and approval to push to production.

# Pare Five :: Ship It!
Phew. You hit the big green Ektron button and proudly shouted "Ship it!", annoying your entire team. Great!

**Next Steps:**
- Let the stakeholders know that the changes have shipped.
- Schedule a time on your calendar or a todo list item for the future to check the metrics you've decided to see if what you shipped is working, or not. This could be weeks or months away.
- Update any documentation if needed. For example, is this a design solution that other marketers would find helpful? Or something that could be scaled across all Ektron microsites? 
- If needed, do a reptrospective. Think about what went well, and what didn't. Perhaps something in this guide needs to be adjusted? Go for it! Adapt as needed.
