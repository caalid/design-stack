# Design Stack

## Sketching

* Pencils: Uni Mechanical Pencil, Kuru Toga Pipe Slide Model 0.5mm Lead, Black http://a.co/d/fCPFIuh
* Pens: MUJI Gel Ink Ball Point Pen 0.5mm Black color 10pcs http://a.co/d/byWKpW7
* Markers: Prismacolor 3620 Premier Double-Ended Art Markers, Fine and Chisel Tip http://a.co/d/4pDGriZ
* Markers: Copic Sketch Twin Tip Marker Pen - C3 Cool Grey 3 http://a.co/d/frqS7xp
* Notebooks: Rhodia Staplebound Pad No.18 - A4 (8.25 x 11.75 inches), Graph, Black http://a.co/d/dza9qbD
* Tiny Notebook: Field Notes Kraft Graph 3-Pack http://a.co/d/bfHMsyV

## Wireframing & Visual Design
I use Sketch as my primary, daily-driver for wireframing and hi-fidelity mockups and comps. It's fast, inexpensive, and widely used by UI designers, UX designers, and Product Designers. Other comparable tools include Adobe XD, Balsamiq (geared more towards simple wireframing), or Axure (geared more towards complex, interactive wireframes).

I use Sketch Libraries for a simple design system, and use it as a linked library in other Sketch documents.

Lately, I've found less of a need for traditional wireframing, and have instead done whiteboarding and/or sketching directly to hi-fi mockups and prototypes.

I'll often use Keynote to prep decks and present to stakeholders when needed, but I find that an InVision prototype or board is easier. You can walk people through it at the meeting, and then they can leave comments directly on it later.

See Sketch Plugins for an overview of the plugins I tend to use with Sketch.

## Version Control
Lately, I've been using Abstract for version control. Abstract is basically git for designers -- it allows you to track changes to a Sketch file over time, and create branches for explorations. Abstract is a paid product, but the free tier allows you to use the product as a solo designer.

For ad-hoc version control, I'll often simply use an `_archive` page in Sketch and drag artboards into it. For bigger changes, I simply duplicate and rename the Sketch file.

## Prototyping
My go-to for prototyping and sharing with stakeholders is InVision. It offers easy-to-use clickable prototypes, and easily lets stakeholders get up to speed with tour points, and leave comments directly on the prototype. Ideally, as much conversation around the prototype would happen in InVision. I use Craft Sync to sync artboards to InVision prototypes directly from Sketch.

For more intricate prototypes, I use Framer. While complex, Framer allows you to create more complex, interactive prototypes complete with animations. Plus, stakeholders can interact with the prototype on through their web browser or mobile device. Framer can sync your assets from Sketch, and then you can use Coffeescript to animate your prototype.

For animation concepts, I use Principle or After Effects. The output for these is typically a video file or GIF showing a quick example of an animation, like a menu opening, or a concept. There is an open-source plugin called Sketch2AE that can sync Sketch assets into After Effects, so you can avoid exporting rendered assets and importing them into After Effects manually -- making it much easier to update things like text or colors, for example, inside After Effects if your design changes.

I also use **Overflow** for creating user flows and diagrams. It can sync hi-fi screens or wireframes with Sketch, so your flows can stay up-to-date. Plus, you can send a web link to stakeholders that you can continue to update.

## Handoff & Specs
Nothing replaces talking with, and collaboring closely with, developers. But, typically, I'll use a few tools:

* InVision offers a tool called Inspect, which creates developer specs from artboards. Since it's tied to your prototype, you can simply update your artboards in Sketch and update the developer specs all at once.

* Sometimes, you have to get in there and redline. Highlight as much or as little as your developer needs to understand your intent. Add notes next to each artboard as needed.

* Keyframe details for animations, in addition to sending GIFS or movie files from Principle or After Effects, or an interactive Framer prototype. It's helpful to lay out visually what your animation will be doing in detail.

## Measuring
* I work closely with Nick to analyze Google Analytics and measurements post-shipping.
* I often use PSPP, which is an open-source statistical analysis application, to analyze larger datasets, like purchase or checkout data. It's useful for creating things like crosstabs, bivariate correlations, or weighting data.
* It's ofen useful to spin up a simple SQL database locally to run queries for analyzing data as well. I use an open source application called DB Browser for SQLite to spin up SQlite databases.

## Other Helpful Applications
* Littleipsum is a helpful little Mac app that lives in the menubar and copies various lengths of Lorem Ipsum text to the clipboard.
* aText is a helpful menubar app that can expand text. I use to expand things like `;date` into `2018-09-11` (the current date) for example.
* Cyberduck to access CUNA's FTP server.
* Annotate to annotate screenshots. It lives in the Mac menubar.
* Nuceleo to manage icon sets. We use Fontawesome as an icon font on cuna.org.
* Cloudapp to quickly share screenshots over email or Skype.
* Pixelsnap to measure elements on-screen using crosshairs.
* Simplemind Lite is a free Mac app to generate diagrams.
* Beardedspice lets you control things like podcasts in the browser using the Mac keyboard media keys.
* Visual Studio Code for editing code and markdown documents.
* ImageOptim for compressing and optimizing any image assets.
* Hyper as a replacement for the default Mac terminal.
* Copyclip, which lives in the menubar as a way to access clipboard history.
* Markoff to preview Markdown files.
* Caffeine is a free Mac app that lives in the menubar and let's you keep your Mac from sleeping.
* OneDrive to sync all my projects up to my CUNA OneDrive account.
* Post Haste lets you create file and folder structures as templates that you can duplciate per-project.
* Wake is a tool that is connected to our InVision account. It lets you post quick, in-progress snapshots of your work to different boards.

## CUNA Design Playbook
See the detailed README in the CUNA Design Playbook repository on Gitlab for detailed information.

## UX Go-Bag Supplies
Everything you need to run a great workshop or design studio with stakeholders.

Post-it® Self-Stick Easel Pad, 25" x 30", Plain White Paper, 30 Sheets Item (#618017)
https://www.officedepot.com/a/products/618017/Post-it-Self-Stick-Easel-Pad/
FOR: Big sticky sheets to hang on walls to group post-its, cards, or screens
 
Sharpie® Permanent Fine-Point Markers, Black, Pack Of 12 Item (#203349)
https://www.officedepot.com/a/products/203349/Sharpie-Permanent-Fine-Point-Markers-Black/
FOR: Making sure we have our own stash for workshops
 
EXPO® Click Fine-Point Dry-Erase Markers, Assorted, Pack Of 6 Item (#768915)
https://www.officedepot.com/a/products/768915/EXPO-Click-Fine-Point-Dry-Erase/
FOR: Ensuring we have enough colors on hand and working markers for workshops
 
Office Depot® Brand Blank Index Cards, 3" x 5", White, Pack Of 300 Item (#1397809)
https://www.officedepot.com/a/products/1397809/Office-Depot-Brand-Blank-Index-Cards/
FOR: Noting and voting and participant involvement
 
Office Depot® Brand Ruled Rainbow Index Cards, 3" x 5", Assorted Colors, Pack Of 100 Item (#1395064)
https://www.officedepot.com/a/products/1395064/Office-Depot-Brand-Ruled-Rainbow-Index/
FOR: Card sorting and participant involvement (multi-colored cards helps)
 
Post-it® Super Sticky Notes, 3" x 3", Rio de Janeiro Collection, 90 Notes Per Pad, Pack Of 24 Pads Item (#386151)
https://www.officedepot.com/a/products/386151/Post-it-Super-Sticky-Notes-3/
FOR: Many things! So many things!
 
Office Depot® Brand Removable Round Color-Coding Labels, OD98803, 1/4" Diameter, Multicolor Dots, Pack Of 768 Item (#837603)
https://www.officedepot.com/a/products/837603/Office-Depot-Brand-Removable-Round-Color/
FOR: Note-and-vote with participants when screenshots, post-its, or cards are on the wall or whiteboard
 
Koh-I-Noor Drafting Dots - Paper - Self-adhesive, Removable, Residue-free - Dispenser Included - 1 / Box - White Item (#446745)
https://www.officedepot.com/a/products/446745/Koh-I-Noor-Drafting-Dots-Paper/
FOR: Drafting dots for hanging screenshots or other things to walls
 
Office Depot® Brand Pushpins, Round, 1/2", Clear, Pack Of 200 Item (#825265)
https://www.officedepot.com/a/products/825265/Office-Depot-Brand-Pushpins-Round-12/
FOR: Hanging screens, sheets, post-its, etc to digital work area

Find It Supply Caddy, 8.75 x 12 Inches, Canvas, 6 Pockets, 6 Compartments, 10 Storage Loops, Black (FT07201)
http://a.co/d/1BhX01v
FOR: Bag to keep everything in
