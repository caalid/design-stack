# cuna.org breakpoints

* 1366 and above :: Desktop XL
* 1365 - 1201 :: Desktop L (full width header/footer/container)
* 992 - 1200 :: Desktop M
* 768 - 991 :: Desktop S
* 0 - 767 :: Tablet
