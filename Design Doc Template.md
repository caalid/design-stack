
## Current State
- [ ] Define problem, jobs, and design goals
- [ ] Outline system and UX exploration
- [ ] UI & production
- [ ] Design complete

## Context
Why is your team devoting time to this project? How does it fit into the bigger picture? How does it relate to past projects?

## Problems
List your problems from highest to lowest priority if possible. Keep them concise.
1. Problem - Description
2. Problem - Description
3. Problem - Description
If possible - link to any research findings here

## Jobs
Jobs help you understand the situations, motivations, and expected outcomes for users who'd encounter your solutions to the problem. There's a structured syntax for writing jobs which looks like this:

When [situation]
I want to [motivation]
So that I can [expected outcome]

When I'm not at a place of work
I want to know when and why one of my customers is about to churn
So that I can take immediate action to try and remedy their issue

It also helps to list out roles with jobs - for instance an engineer might want to see different information than a marketing manager for the situation above. Think about who will be interacting with your solution. Don't go too deep, these aren't personas. Just a way to categorize different situations.

## Design Goals
Design goals are the first part to a solution. What are you going to try to accomplish given the stated problems, users, and jobs? At a very high level, what will your solutions bring to the table? Don't get too detailed in terms of what the solution might look like (i.e. send a push notification to the user because x,y,z).

Examples:
1. Help the user discover more ways to save money while increasing their overall engagement
2. Keep users updated on when their about to lose out on vouchers/offers/credits (expiration or eligibility to redeem)
3. Make it easy for users to redeem credit within the app coming from offsite content

## Current System
Your solution will almost always be touching upon an existing system with entry and exit points, one which is made of different components with dependencies and relationships to each other, etc. Take everything apart, understand how the existing system might effect your solutions and know the benefits or repercussions of any additions or modifications to the system.

High level diagrams, inventories, and listing out terminology help you, your team, and others come to a common understanding.

## Explore
You're ready to start thinking about solutions! The exciting part...let's be honest. However, before diving into pixels and execution - it's useful to brainstorm and jot down initial thoughts around solutions that no doubt you've had in the back of your mind. List a few out in diagrams, wireframes, low-fidelity mocks - and write pros and cons for each. How do they fit into the existing system? How well do they solve the initial problems and cater around the jobs?

Title [Option A, etc]
Example: [Mock]
Pros - Why does this work well?
Cons - Potential concerns?

## Execute
Now it's time to visualize the chosen solutions through high-fidelity mocks and prototypes. Break your solution(s) down into concrete sections which reference back to the problems and jobs. Which problems do they solve and how do they relate to your original design goals?

