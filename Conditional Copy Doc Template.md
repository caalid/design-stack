# Conditional Copy Doc

Useful for handing off things like error message copy, or other instances where there are multiple states.

|State|Client or Server|Conditions|Copy|
|---|---|---|---|
|Default|Server|No excluded items in user’s cart|As a CUNA member, you are able to save on this order. Additional discounts will be automatically applied during checkout. Exclusions may apply.|
