# File Organization
---


## Projects Folder
An example organization inside the projects folder.

### Main Folders
* `01-projects` for projects that do not have a product area
* `02-mini-initiatives` is used just for right now to group projects for mini-initiatives. Same with `03-initiatives` and `04-releases`. Once there is more established clarity around Product Areas, Modules, and Sub-Modules, I would recommend not organizing in this way.
* `05-systems` for projects like design systems or email toolkits, which are across multiple project areas.

### Resources & Archive Folders
Any folder inside these can have a `-resources` folder. We use the `-` to keep the folder at the top. Similarly, we use `-archive` to keep mini trash cans of old work. You never know when someone will want to see something that you designed 4 weeks ago. Keep it around!

### Product Area
The main prodouct or product area. This could be divided by platform (iOS, web, desktop) or otherwise. Product Areas for CUNA are TBD.

`My-CUNA-account` could be a Product Area. Or, `CPD-Online`, or `iOS-App`. 

### Milestones
The milestone or version number for that product or product area. This could be a release date or number, or other version number. Namespacing by Year-Month-Date is helpful for sorting, like `2018-08-10` or `2018-08-Aug` for months.

`(m)-0.0` or `(m)-2018-08-august-release` or whatever works for your team/org.

### Modules
A module is a breakdown of each module inside the product or product area and milestone. For example, "Account Signup" could be module.

`(p)-module-name`

### Sub-Modules
Optionally, a module could be broken down into sub-modules. "Account Signup" could have "Forms" as a sub-module. Not all modules need sub-modules.

`(pp)-sub-module-name`

### Epics
An epic is a group of related user stories inside a milestone/release. A module or submodule could have multiple epics inside of it. We also can include a `-resources` folder, prefixed with `-` to keep it at the top. **You can also completely skip this and put stories directly into sub-modules.**

`(e)-epic-name`

### User Stories
Epics are broken down into multiple user stories. Optionally, this folder can have a `-resources` folder as well.

`(s)-story-name`

### Deliverables & Outputs (Tasks)
Finally, each user story has deliverables and activities. We represent these by pre-fixing with `(t)`.

**Naming:**
* Always dash-separated
* Short and clear, but understandable. Be careful with abbrevations.
* `deliverable-title`-`deliverable-type`-`v01a`.`extension`
    * `v01a` speaks to version and ideation

* `(t)-animations` :: Any animations or prototypes made with Principle or After Effects.
* `(t)-mockups` :: Hi-Fi sketch files go here
* `(t)-personas` :: User research personas
* `(t)-sketches` :: Any scans of sketches
* `(t)-wireframes` :: Wireframes
* `(t)-flows` :: User flows
* `(t)-decks` :: Any decks as deliverables
* `(t)-requirements` :: Any project requirement documentation
* `(t)-research` :: User research
* `(t)-references` :: Thingsl like benchmarks, inspiration, etc
* `(t)-exports` :: Working hi-fi screen exports, for use in decks, deliverables, etc.
* `(t)-development` :: Any files for handoffs to engineering
* `-docs` :: Any project documentation that is internal-facing
* `-assets` :: Any created assets, like vectors, graphics, edited photos, etc. Organized inside by type.

## Tracking Screens
* Keep names short yet descriptive.
* Organize by section, like:
    * `00a-home-desktop.png`
    * `01a-articles-listing-mobile.png`
    * `01b-articles-listing-filtered-mobile.png`
    * Etc, Etc.